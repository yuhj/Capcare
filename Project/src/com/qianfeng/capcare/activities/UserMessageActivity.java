package com.qianfeng.capcare.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qianfeng.capcare.R;
import com.qianfeng.capcare.utils.DialogUtils;

/**
 * 
 * @author 郭鹏飞
 * @version 1.0
 * @date 14-10-10
 * 
 */
public class UserMessageActivity extends Activity implements OnClickListener{

	/**
	 * 头部布局
	 */
	private View view;
	/**
	 * 头部布局的返回
	 */
	private ImageView ivBack;
	/**
	 * 头部布局的确定
	 */
	private TextView tvConfirm;

	/**
	 * 车辆信息设置控件
	 */
	private TextView tvUserNick, tvUserBirthday, tvUserSex,
			tvUserHeight, tvUserWeight;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_user_message);

		// 根据id获取到实例对象
		view = findViewById(R.id.capcare_bindingequipment_title);

		ivBack = (ImageView) view
				.findViewById(R.id.capcare_bindingequipment_title_back);
		tvConfirm = (TextView) view
				.findViewById(R.id.capcare_bindingequipment_title_confirm);

		tvUserBirthday = (TextView) findViewById(R.id.capcare_bindingequipment_user_birthday_txt_id);
		tvUserHeight = (TextView) findViewById(R.id.capcare_bindingequipment_user_height_txt_id);
		tvUserNick = (TextView) findViewById(R.id.capcare_bindingequipment_user_nick_txt_id);
		tvUserSex = (TextView) findViewById(R.id.capcare_bindingequipment_user_sex_txt_id);
		tvUserWeight = (TextView) findViewById(R.id.capcare_bindingequipment_user_weight_txt_id);

		// 为控件添加点击事件的监听
		ivBack.setOnClickListener(this);
		tvConfirm.setOnClickListener(this);
		
		tvUserBirthday.setOnClickListener(this);
		tvUserHeight.setOnClickListener(this);
		tvUserNick.setOnClickListener(this);;
		tvUserSex.setOnClickListener(this);
		tvUserWeight.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.capcare_bindingequipment_title_back:
			// TODO 处理点击返回上一个界面
			finish();
			break;
		case R.id.capcare_bindingequipment_title_confirm:
			// TODO 处理点击确定事件
			Toast.makeText(getApplicationContext(), "确定", 1).show();
			break;
		case R.id.capcare_bindingequipment_user_birthday_txt_id:
			DialogUtils.showDialog(this, "生日", tvUserBirthday);
			break;
		case R.id.capcare_bindingequipment_user_height_txt_id:
			DialogUtils.showDialog(this, "身高", tvUserHeight);
			break;
		case R.id.capcare_bindingequipment_user_nick_txt_id:
			DialogUtils.showDialog(this, "名称", tvUserNick);
			break;
		case R.id.capcare_bindingequipment_user_sex_txt_id:
			DialogUtils.showDialog(this, "性别", tvUserSex);
			break;
		case R.id.capcare_bindingequipment_user_weight_txt_id:
			DialogUtils.showDialog(this, "体重", tvUserWeight);
			break;
		default:
			break;
		}
	}

}
