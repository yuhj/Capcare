package com.qianfeng.capcare.activities;

import com.qianfeng.capcare.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @name ScanningEquipmentActivity
 * @Descripation
				这是用来绑定设备附带系信息的类<br>
				包含两个内容<br>
				1.SIM<br>
				2.报警电话<br>
 * @author 禹慧军
 * @email lin5667181@163.com
 * @date 2014-10-11
 * @version 1.0
 */
public class ScanningEquipmentActivity extends Activity implements OnClickListener{
	private TextView iMMeTextView;
	private Button btn_SIM;
	private Button btn_Phone;
	private ImageButton btn_complete;
	private ImageButton btn_back;
	private String iMIE;
	private String sIM;
	private String pHONE;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.capcare_bindingequipment_edit);
		initViews();
		Intent intent =getIntent();
		
				iMIE = intent.getStringExtra("IMIE");
				iMMeTextView.setText(iMIE);
	
			
	}
	private void setPhone(Intent intent) {
		String type = intent.getStringExtra("type");	
		if ("SIM".equals(type)) {
			sIM = intent.getStringExtra("phone")+"\t";
			btn_SIM.setText(sIM);
		}else if ("PHONE".equals(type)) {
			pHONE = intent.getStringExtra("phone")+"\t";
			btn_Phone.setText(pHONE);
		}
	}
	private void initViews() {
		iMMeTextView=(TextView) findViewById(R.id.capcare_bindingequipment_text_IMIE);
		btn_Phone=(Button) findViewById(R.id.capcare_bindingeqipment_contastPhone);
		btn_SIM=(Button) findViewById(R.id.capcare_bindingeqipment_SimId);
		btn_complete=(ImageButton) findViewById(R.id.capcare_bingequp_imgbtn_complete);
		btn_back=(ImageButton) findViewById(R.id.capcare_bingequp_imgbtn_goback);
		btn_back.setOnClickListener(this);
		btn_complete.setOnClickListener(this);
		btn_Phone.setOnClickListener(this);
		btn_SIM.setOnClickListener(this);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("IMME",iMIE);
		outState.putString("SIM",sIM);
		outState.putString("PHONE",pHONE);
	}
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.capcare_bindingeqipment_contastPhone:
			Intent intent_phone =new Intent(ScanningEquipmentActivity.this,EditEquipmentSimPhoneActivity.class);
			intent_phone.putExtra("title","PHONE");
			intent_phone.putExtra("IMIE",iMIE);
			startActivityForResult(intent_phone,1001);
			break;
		case R.id.capcare_bindingeqipment_SimId:
			Intent intent_sim =new Intent(ScanningEquipmentActivity.this,EditEquipmentSimPhoneActivity.class);
			intent_sim.putExtra("title","SIM");
			intent_sim.putExtra("IMIE",iMIE);
			startActivityForResult(intent_sim, 1002); 
			break;
		case R.id.capcare_bingequp_imgbtn_complete:
			break;
		case R.id.capcare_bingequp_imgbtn_goback:
			startActivity(new Intent(ScanningEquipmentActivity.this,BindingEquipmentActivity.class));
			break;

		default:
			break;
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (data!=null) {
			if (requestCode==1001) {
				String iMIE=data.getStringExtra("IMIE");
				setPhone(data);
				iMMeTextView.setText(iMIE);
			}else if (requestCode==1002) {
				String iMIE=data.getStringExtra("IMIE");
				setPhone(data);
				iMMeTextView.setText(iMIE);
			}
		}
		
	}
	


}
