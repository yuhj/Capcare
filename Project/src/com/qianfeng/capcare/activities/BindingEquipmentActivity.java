package com.qianfeng.capcare.activities;

import com.qianfeng.capcare.R;
import com.zbar.lib.CaptureActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.ViewDebug.FlagToString;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * @name BindingEquipmentActivity
 * @Descripation
				这是一个用来绑定设备的类<br>
				绑定设备有两种方式<br>
				1.输入IMEI码<br>
				2.用二维码扫描<br>
 * @author 禹慧军
 * @email lin5667181@163.com
 * @date 2014-10-10
 * @version 1.0
 */
public class BindingEquipmentActivity extends Activity implements OnClickListener{
	private EditText input_IMME;
	private Button btn_erweima;
	private ImageButton btn_complete;
	private Button btn_htlp;
	private String iMIE;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.capcare_bindingequipment_main);
		initViews();
	}

	private void initViews() {
		input_IMME=(EditText) findViewById(R.id.capcare_bindingequipment_input_imie_edittext);
		btn_erweima=(Button) findViewById(R.id.capcare_bindingequipment_btn_erweima);
		btn_complete=(ImageButton)findViewById(R.id.capcare_bingequp_imgbtn_complete);
		btn_htlp=(Button) findViewById(R.id.capcare_bindingequipment_btn_addequipment_help);
		btn_complete.setOnClickListener(this);
		btn_erweima.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.capcare_bindingequipment_btn_erweima:
			//扫描二维码界面，跳转
			Intent erweimaIntent =new Intent(BindingEquipmentActivity.this,CaptureActivity.class);
			startActivity(erweimaIntent);
			
			
			break;
		case R.id.capcare_bingequp_imgbtn_complete:
			iMIE = input_IMME.getText().toString().trim();
			if (iMIE.length()==15) {
				if (iMIE.matches("[0-9]+")) {
					//跳转到扫描设备信息界面
					Intent intent =new Intent(BindingEquipmentActivity.this,ScanningEquipmentActivity.class);
					intent.putExtra("IMIE",iMIE);
					startActivity(intent);
				}else {
					Toast.makeText(getApplicationContext(),"您输入的IMIE码非法，请检查!", 1).show();
				}
				
			}else {
				Toast.makeText(getApplicationContext(),"您输入的IMME码长度有误，请检查!", 1).show();
			}
			
			break;
			case R.id.capcare_bindingequipment_btn_addequipment_help:
				//跳转到绑定设备帮助界面
				break;
		default:
			break;
		}
		
	}

}
