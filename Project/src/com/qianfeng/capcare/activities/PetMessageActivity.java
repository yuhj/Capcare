package com.qianfeng.capcare.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qianfeng.capcare.R;
import com.qianfeng.capcare.utils.DialogUtils;

/**
 * 
 * @author 郭鹏飞
 * @version 1.0
 * @date 14-10-10
 * 
 */
public class PetMessageActivity extends Activity implements OnClickListener{

	/**
	 * 头部布局
	 */
	private View view, picture;
	/**
	 * 头部布局的返回
	 */
	private ImageView ivBack;
	/**
	 * 头部布局的确定
	 */
	private TextView tvConfirm;

	/**
	 * 车辆信息设置控件
	 */
	private TextView tvPetName, tvPetVariety, tvPetType,
			tvPetBirthday, tvPetSex, tvPetHeight, tvPetWeight;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_pet_message);

		// 根据id获取到实例对象
		view = findViewById(R.id.capcare_bindingequipment_title);
		picture = findViewById(R.id.capcare_bindingequipment_pet_picture_id);
		
		ivBack = (ImageView) view
				.findViewById(R.id.capcare_bindingequipment_title_back);
		tvConfirm = (TextView) view
				.findViewById(R.id.capcare_bindingequipment_title_confirm);

		tvPetBirthday = (TextView) findViewById(R.id.capcare_bindingequipment_pet_birthday_txt_id);
		tvPetHeight = (TextView) findViewById(R.id.capcare_bindingequipment_pet_height_txt_id);
		tvPetName = (TextView) findViewById(R.id.capcare_bindingequipment_pet_name_txt_id);
		tvPetSex = (TextView) findViewById(R.id.capcare_bindingequipment_pet_sex_txt_id);
		tvPetType = (TextView) findViewById(R.id.capcare_bindingequipment_pet_type_txt_id);
		tvPetVariety = (TextView) findViewById(R.id.capcare_bindingequipment_pet_variety_txt_id);
		tvPetWeight = (TextView) findViewById(R.id.capcare_bindingequipment_pet_weight_txt_id);
		
		// 为控件添加点击事件的监听
		picture.setOnClickListener(this);
		
		ivBack.setOnClickListener(this);
		tvConfirm.setOnClickListener(this);
		
		tvPetBirthday.setOnClickListener(this);
		tvPetHeight.setOnClickListener(this);
		tvPetName.setOnClickListener(this);;
		tvPetSex.setOnClickListener(this);
		tvPetType.setOnClickListener(this);
		tvPetVariety.setOnClickListener(this);;
		tvPetWeight.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.capcare_bindingequipment_title_back:
			// TODO 处理点击返回上一个界面
			finish();
			break;
		case R.id.capcare_bindingequipment_title_confirm:
			// TODO 处理点击确定事件
			Toast.makeText(getApplicationContext(), "确定", 1).show();
			break;
		case R.id.capcare_bindingequipment_pet_picture_id:
			Intent intent = new Intent(this, SetPictureActivity.class);
			intent.putExtra("title", "宠物头像");
			startActivity(intent);
			break;
		case R.id.capcare_bindingequipment_pet_birthday_txt_id:
			DialogUtils.showDialog(this, "宠物生日", tvPetBirthday);
			break;
		case R.id.capcare_bindingequipment_pet_height_txt_id:
			DialogUtils.showDialog(this, "宠物身高", tvPetHeight);
			break;
		case R.id.capcare_bindingequipment_pet_name_txt_id:
			DialogUtils.showDialog(this, "宠物小名", tvPetName);
			break;
		case R.id.capcare_bindingequipment_pet_sex_txt_id:
			DialogUtils.showDialog(this, "宠物性别", tvPetSex);
			break;
		case R.id.capcare_bindingequipment_pet_type_txt_id:
			DialogUtils.showDialog(this, "宠物类型", tvPetType);
			break;
		case R.id.capcare_bindingequipment_pet_variety_txt_id:
			DialogUtils.showDialog(this, "宠物品种", tvPetVariety);
			break;
		case R.id.capcare_bindingequipment_pet_weight_txt_id:
			DialogUtils.showDialog(this, "宠物体重", tvPetWeight);
			break;
		default:
			break;
		}
	}

}
