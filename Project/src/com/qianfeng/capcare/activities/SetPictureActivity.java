package com.qianfeng.capcare.activities;

import com.qianfeng.capcare.R;

import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * 
 * @author Administrator
 * 
 */
public class SetPictureActivity extends Activity implements OnClickListener {

	private GridView gvPictures;
	private Cursor cursor;

	private RadioButton rbtnCamera, rbtnGallery;
	
	private TextView tvTitle, tvConfirm;
	private ImageView ivBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_bindingequipment_get_picture);

		/**
		 * 获取实例
		 */
		gvPictures = (GridView) findViewById(R.id.capcare_bindingequipment_show_picture_gv_id);

		rbtnCamera = (RadioButton) findViewById(R.id.capcare_bindingequipment_take_picture_id);
		rbtnGallery = (RadioButton) findViewById(R.id.capcare_bindingequipment_get_pictre_id);

		tvConfirm = (TextView) findViewById(R.id.capcare_bindingequipment_title_confirm);
		ivBack = (ImageView) findViewById(R.id.capcare_bindingequipment_title_back);
		/**
		 * 设置标题
		 */
		tvTitle = (TextView) findViewById(R.id.capcare_bindingequipment_title_message);
		Intent intent = getIntent();
		String title = intent.getStringExtra("title");
		tvTitle.setText(title);
		
		/**
		 * 注册监听
		 */
		rbtnCamera.setOnClickListener(this);
		rbtnCamera.setOnClickListener(this);
		
		tvConfirm.setOnClickListener(this);
		ivBack.setOnClickListener(this);

		cursor = getContentResolver().query(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null,
				null);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.capcare_bindingequipment_title_back:
			// TODO 返回上一个界面
			finish();
			break;
		case R.id.capcare_bindingequipment_title_confirm:
			// TODO 选定照片
			break;
		case R.id.capcare_bindingequipment_get_pictre_id:
			// TODO 从本地获取图片
			break;
		case R.id.capcare_bindingequipment_take_picture_id:
			// TODO 开启照相机
			Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
			startActivityForResult(intent, 0);
			break;
		default:
			break;
		}
	}

	/**
	 * 拍照成功以后返回做处理
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
	}
}
