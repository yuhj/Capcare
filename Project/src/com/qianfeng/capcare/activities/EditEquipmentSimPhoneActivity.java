package com.qianfeng.capcare.activities;

import com.qianfeng.capcare.R;

import android.R.integer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @name EditEquipmentSimPhoneActivity
 * @Descripation 这是用来编辑SIM号和报警电话的类<br>
 * @author 禹慧军
 * @email lin5667181@163.com
 * @date 2014-10-11
 * @version 1.0
 */
public class EditEquipmentSimPhoneActivity extends Activity implements
		OnClickListener {
	private EditText Country;
	private EditText phone;
	private ImageButton btn_complete;
	private ImageButton btn_back;
	private String countryId;
	private String phoneNum;
	private TextView titleTextView;
	private String title;
	private String iMIE;
	private int RequltCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.capcare_bindingequipment_bind_sim_and_phone);
		initViews();
		
		
		Intent intent =getIntent();
		title =intent.getStringExtra("title");
		if (title!=null) {
			if ("SIM".equals(title)) {
				titleTextView.setText("绑定SIM卡号");
				RequltCode=1002;
			}else if ("PHONE".equals(title)) {
				titleTextView.setText("绑定报警电话");
				RequltCode=1001;
			}
		}
		iMIE = intent.getStringExtra("IMIE");
		
		
	}

	private void initViews() {
		Country = (EditText) findViewById(R.id.capcare_bindingequipment_input_coutntry);
		phone = (EditText) findViewById(R.id.capcare_bindingequipment_input_phone);
		btn_back = (ImageButton) findViewById(R.id.capcare_bingequp_imgbtn_goback);
		btn_complete = (ImageButton) findViewById(R.id.capcare_bingequp_imgbtn_complete);
		titleTextView=(TextView) findViewById(R.id.capcare_bingequp_text_bindequip);
		btn_back.setOnClickListener(this);
		btn_complete.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.capcare_bingequp_imgbtn_complete:
			phoneNum = phone.getText().toString().trim();
			countryId = Country.getText().toString().trim();
			if (phoneNum!=null&&countryId!=null) {
				if (phoneNum.matches("[0-9]{11}")) {
					Intent intent =new Intent(EditEquipmentSimPhoneActivity.this,ScanningEquipmentActivity.class);
					intent.putExtra("phone",phoneNum);
					intent.putExtra("type",title);
					intent.putExtra("IMIE", iMIE);
					setResult(RequltCode,intent);  
		            //关闭掉这个Activity  
		            finish();
				}else {
					Toast.makeText(getApplicationContext(),"您输入的不是一个手机号，请确认后重新输入！",1).show();
				}
				
			}else {
				Toast.makeText(getApplicationContext(), "请完善信息！",1).show();
			}
			
			break;
		case R.id.capcare_bingequp_imgbtn_goback:
			startActivity(new Intent(EditEquipmentSimPhoneActivity.this,ScanningEquipmentActivity.class));
			break;

		default:
			break;
		}

	}

}
